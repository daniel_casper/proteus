# Proteus

Proteus
An internal template editor which also renders changes in the browser as a preview. Can be expanded to manage campaigns and instances.
Specifications
Scope
/clients show clients
/clients/:client-id show client info
/clients/:client-id/profiles show profiles
/clients/:client-id/profiles/:profile-id import edit remove preview
/clients/:client-id/campaigns shows all campaigns create new campaign delete a campaign {{#each campaign}} {{link-to /:campaign_id}} {{/each}}
/clients/:client-id/campaigns/:campaign_id shows instances with campaign add instance get an instance delete instance edit campaign shows/previews profile/quick edit profile
/clients/:client-id/campaigns/:campaign_id/instances shows more info about instances edit / delete instances
/clients/:client-id/campaigns/:campaign_id/instances/:instance_id edit/create instance details campaign (move?) instance name date/name subject
/clients/:client-id/campaigns/:campaign_id/instances/:instance_id/templates new template saved/previous templates
/clients/:client-id/campaigns/:campaign_id/instances/:instance_id/templates/:template_id basic themes (default templates) custom code modify previous template preview
/clients/:client-id/campaigns/:campaign_id/instances/:instance_id/review

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with NPM)
* [Bower](http://bower.io/)
* [Ember CLI](http://www.ember-cli.com/)
* [PhantomJS](http://phantomjs.org/)

## Installation

* `git clone <repository-url>` this repository
* change into the new directory
* `npm install`
* `bower install`

## Running / Development

* `ember server`
* Visit your app at [http://localhost:4200](http://localhost:4200).

### Code Generators

Make use of the many generators for code, try `ember help generate` for more details

### Running Tests

* `ember test`
* `ember test --server`

### Building

* `ember build` (development)
* `ember build --environment production` (production)

### Deploying

Specify what it takes to deploy your app.

## Further Reading / Useful Links

* [ember.js](http://emberjs.com/)
* [ember-cli](http://www.ember-cli.com/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)

